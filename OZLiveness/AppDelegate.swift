//
//  AppDelegate.swift
//  OZLiveness
//
//  Created by Oz Forensics LLC. on 01/07/2019.
//  Copyright © 2019 Oz Forensics LLC. All rights reserved.
//

import UIKit
import OZLivenessSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        UIApplication.shared.isIdleTimerDisabled = true
        
        OZSDK(licenseSources: [.licenseFileName("forensics.license")]) { (result, error) in
            if let error = error {
                print(error)
                // license error
            }
        }
        OZSDK.journalObserver = { (value : String) -> () in
            //
        }
        
        let hintAnimationCustomization: HintAnimationCustomization = HintAnimationCustomization(
            hideAnimation: false,
            animationIconSize: 80,
            hintGradientColor: .green
        )
        
        let centerHintCustomization: CenterHintCustomization = .init(
            verticalPosition: 10,
            hideTextBackground: true
        )
        
        let faceFrameCustomization: FaceFrameCustomization = FaceFrameCustomization(
            strokeWidth: 4,
            strokeFaceAlignedColor: UIColor.systemGreen,
            strokeFaceNotAlignedColor: UIColor.red,
            geometryType: .oval,
            strokePadding: 0
        )
        
        let backgroundCustomization = BackgroundCustomization(
            backgroundColor: UIColor.darkGray.withAlphaComponent(0.85)
        )
        
        OZSDK.customization.faceFrameCustomization = faceFrameCustomization
        OZSDK.customization.hintAnimationCustomization = hintAnimationCustomization
        OZSDK.customization.backgroundCustomization = backgroundCustomization
        OZSDK.customization.centerHintCustomization = centerHintCustomization
        
        
        OZSDK.localizationCode = .en
        Router.showFirstController()
        
        return true
    }
}

