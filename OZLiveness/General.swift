//
//  General.swift
//  OZLiveness
//
//  Created by Oz Forensics LLC. on 12/08/2019.
//  Copyright © 2019 Oz Forensics LLC. All rights reserved.
//

import Foundation
import UIKit
import OZLivenessSDK

func localized(_ key: String) -> String {
    return NSLocalizedString(key,
                             tableName: "Localizable",
                             bundle: Bundle.main,
                             comment: "")
}

struct UIElements {
    private init() { }
    static var activityView : InfoView {
        get {
            let frame : CGRect = UIApplication.shared.keyWindow?.rootViewController?.view.frame ?? .zero
            let activityView = InfoView(frame: frame)
            activityView.addSubview(activityView.label)
            activityView.label.textAlignment = .center
            activityView.label.textColor = UIColor.black
            activityView.label.frame = CGRect(
                x: frame.origin.x,
                y: frame.origin.y + frame.height / 4,
                width: frame.width,
                height: frame.height / 4
            )
            let activity = UIActivityIndicatorView(frame: CGRect(
                x: frame.origin.x,
                y: frame.origin.y + frame.height / 2,
                width: frame.width,
                height: frame.height / 6
            ))
            activity.startAnimating()
            activity.color = UIColor.gray
            activityView.backgroundColor = UIColor.white.withAlphaComponent(0.9)
            activityView.addSubview(activity)
            return activityView
        }
    }
}

class InfoView : UIView {
    var label : UILabel = UILabel()
}

struct Router {
    
    private(set) static var window: UIWindow?
    
    private init() { }
    
    static func showFirstController() {
        self.showVC(vcID: OZSDK.isLoggedIn() ? "ActionsVCID" : "LoginVCID")
    }
    
    static func showActionsController() {
        self.showVC(vcID: "ActionsVCID")
    }
    
    private static func showVC(vcID: String) {
        window = UIApplication.shared.keyWindow ?? UIWindow(frame: UIScreen.main.bounds)
        let mainSb = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainSb.instantiateViewController(withIdentifier: vcID)
        window?.rootViewController = vc
        window?.makeKeyAndVisible()
    }
}

