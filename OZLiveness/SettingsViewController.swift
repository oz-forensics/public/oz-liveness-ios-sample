//
//  SettingsViewController.swift
//  OZLiveness
//
//  Created by Oz Forensics LLC. on 08/08/2019.
//  Copyright © 2019 Oz Forensics LLC. All rights reserved.
//

import UIKit
import OZLivenessSDK

class SettingCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
}

private let numberFormatter : NumberFormatter = {
    let numberFormatter = NumberFormatter()
    numberFormatter.maximumFractionDigits = 16
    numberFormatter.minimumIntegerDigits = 1
    return numberFormatter
}()

class SettingsViewController: UITableViewController, UITextFieldDelegate {
    
    @IBAction func logout(_ sender: Any) {
        OZSDK.logout()
        Router.showFirstController()
    }
}
