//
//  ActionTableViewController.swift
//  OZLiveness
//
//  Created by Oz Forensics LLC. on 22/07/2019.
//  Copyright © 2019 Oz Forensics LLC. All rights reserved.
//

import UIKit
import OZLivenessSDK

enum AnalysisActionType {
    case onDevice
    case serverBased
}

extension OZVerificationMovement {
    
    var text : String {
        switch self {
        case .smile:    return localized("ActionTableViewController.Action.Smile")
        case .eyes:     return localized("ActionTableViewController.Action.Eyes")
        case .up:       return localized("ActionTableViewController.Action.Up")
        case .down:     return localized("ActionTableViewController.Action.Down")
        case .left:     return localized("ActionTableViewController.Action.Left")
        case .right:    return localized("ActionTableViewController.Action.Right")
        case .scanning: return localized("ActionTableViewController.Action.Scanning")
        case .selfie:   return localized("ActionTableViewController.Action.Selfie")
        case .one_shot: return localized("ActionTableViewController.Action.OneShot")
        @unknown default: return ""
        }
    }
}

private let actionCellID    = "ActionCellID"
private let settingsSegueID = "SettingsSegueID"

enum LivenessScenarios {
    case all, bioCheck
    
    var text : String {
        switch self {
        case .all:              return localized("ActionTableViewController.Scenarios.All")
        case .bioCheck:         return localized("ActionTableViewController.Scenarios.BioCheck")
        }
    }
}

class ActionTableViewController: UITableViewController {
    var mode: AnalysisActionType = .onDevice
    
    let actions : [OZVerificationMovement]      = [.smile, .eyes, .up, .down, .left, .right, .scanning, .selfie, .one_shot]
    let livenessScenarios : [LivenessScenarios] = [.bioCheck]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = localized("ActionTableViewController.Actions")
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title:  localized("ActionTableViewController.ActionBack"),
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(backAction(sender:)))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title:  localized("ActionTableViewController.ActionSettings"),
                                                                style: .plain,
                                                                target: self,
                                                                action: #selector(settingsAction(sender:)))
        OZSDK.attemptSettings = OZAttemptSettings(singleCount: 2,
                                                  commonCount: 3)
    }

    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? actions.count : livenessScenarios.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: actionCellID, for: indexPath)
        if indexPath.section == 0 {
            cell.textLabel?.text = actions[indexPath.row].text
        }
        else {
            cell.textLabel?.text = livenessScenarios[indexPath.row].text
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 1 {
            if livenessScenarios[indexPath.row] == .bioCheck {
                self.performSegue(withIdentifier: "BioVerificationSegueID", sender: self)
                return
            }
        } else {
            do {
                let ozLivenessVC : UIViewController
                if indexPath.section == 0 {
                    ozLivenessVC = try OZSDK.createVerificationVCWithDelegate(self, actions: [actions[indexPath.row]])
                }
                else {
                    switch livenessScenarios[indexPath.row] {
                    default:
                        ozLivenessVC = try OZSDK.createVerificationVCWithDelegate(self, actions: actions)
                    }
                }
                
                self.present(ozLivenessVC, animated: true)
            }
            catch let error {
                print("error: \(error)")
            }
        }
    }
    
    @objc func backAction(sender: UIBarButtonItem) {
        Router.showFirstController()
    }
    
    @objc func settingsAction(sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: settingsSegueID, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        switch segue.identifier {
        case "BioVerificationSegueID":
            let vc = segue.destination as? BioVerificationViewController
            vc?.mode = mode
        default:()
        }
    }
    
    func localLiveness(results: [OZMedia]) {
        let activity = ProgressiveActivityView.show(in: self.view)
        let analysisRequest = AnalysisRequestBuilder()
        let analysis = Analysis.init(media: results, type: .quality, mode: .onDevice)
        analysisRequest.addAnalysis(analysis)
        
        analysisRequest.run { status in
            activity.progressView.startProgress(to: CGFloat((status.progressStatus?.progress.fractionCompleted ?? 0) * 100), duration: 0.1)
        } errorHandler: { error in
            print(error)
        } completionHandler: { results in
            activity.hide()
            var msg = ""
            results.analysisResults.forEach { res in
                msg = msg.isEmpty ? res.type : msg + ", " + res.type
            }
            let alertController = UIAlertController.createAlert(
                title: results.resolution.rawValue,
                message: msg
            )
            alertController.addAction(UIAlertAction(title: "Ok",
                                                    style: .cancel,
                                                    handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func remoteLiveness(results: [OZMedia]) {
        let activity = ProgressiveActivityView.show(in: self.view)
        
        let analysisRequest = AnalysisRequestBuilder()
        
        let analysis = Analysis(media: results,
                                type: .quality,
                                mode: .serverBased,
                                params: nil)
        analysisRequest.addAnalysis(analysis)
        
        analysisRequest.run { status in
            activity.progressView.startProgress(to: CGFloat((status.progressStatus?.progress.fractionCompleted ?? 0) * 100), duration: 0.1)
        } errorHandler: { error in
            print(error)
        } completionHandler: { results in
            OZSDK.cleanTempDirectory()
            activity.hide()
            var msg = ""
            results.analysisResults.forEach { res in
                msg = msg.isEmpty ? res.type : msg + ", " + res.type
            }
            let alertController = UIAlertController.createAlert(
                title: results.resolution.rawValue,
                message: msg
            )
            alertController.addAction(UIAlertAction(title: "Ok",
                                                    style: .cancel,
                                                    handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
    }
}

extension ActionTableViewController: OZLivenessDelegate {
    func onError(status: OZVerificationStatus?) {
        switch status {
        case .failedBecauseUserCancelled:
            let alertController = UIAlertController.createAlert(
                title:      localized("attention"),
                message:    localized("ActionTableViewController.Alert.FailedBecauseUserCancelled.Message")
            )
            alertController.addAction(UIAlertAction(title: localized("ok"),
                                                    style: .cancel,
                                                    handler: nil))
            self.present(alertController, animated: true, completion: { })
        case .failedBecauseOfAttemptLimit:
            let alertController = UIAlertController.createAlert(
                title:      localized("attention"),
                message:    localized("ActionTableViewController.Alert.FailedBecauseOfAttemptLimit.Message")
            )
            alertController.addAction(UIAlertAction(title: localized("ok"),
                                                    style: .cancel,
                                                    handler: nil))
            self.present(alertController, animated: true, completion: { })
        case .failedBecauseOfLicenseError(let error):
            let alertController = UIAlertController.createAlert(
                title:      localized("attention"),
                message:    error.localizedDescription
            )
            alertController.addAction(UIAlertAction(title: localized("ok"),
                                                    style: .cancel,
                                                    handler: nil))
            self.present(alertController, animated: true, completion: { })
        default: ()
        }
    }
    
    func onOZLivenessResult(results: [OZMedia]) {
        UIApplication.shared.isIdleTimerDisabled = false
        
        switch mode {
        case .onDevice:
            self.localLiveness(results: results)
        case .serverBased:
            self.remoteLiveness(results: results)
        }
    }
}

