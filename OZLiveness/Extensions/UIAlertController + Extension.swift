//
//  UIAlertController + Extension.swift
//  OZLiveness
//
//  Created by Ilya Postanogov on 22.01.2022.
//  Copyright © 2022 Igor Ovchinnikov. All rights reserved.
//

import UIKit

extension UIAlertController {
    static func createAlert(title: String, message: String?) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.view.tintColor = #colorLiteral(red: 0.168627451, green: 0.6431372549, blue: 0.5803921569, alpha: 1)
        let lightGreenGray   : UIColor   = #colorLiteral(red: 0.3427193165, green: 0.4501506686, blue: 0.4355462492, alpha: 1)
        alert.setTitlet(font:   UIFont.systemFont(ofSize: 16, weight: .bold),       color: lightGreenGray)
        alert.setMessage(font:  UIFont.systemFont(ofSize: 14.0, weight: .medium),   color: lightGreenGray)
        return alert
    }
    
    private func setTitlet(font: UIFont?, color: UIColor?) {
        guard let title = self.title else { return }
        let attributeString = NSMutableAttributedString(string: title)
        if let titleFont = font {
            attributeString.addAttributes([NSAttributedString.Key.font : titleFont],
                                          range: NSMakeRange(0, title.count))
        }
        
        if let titleColor = color {
            attributeString.addAttributes([NSAttributedString.Key.foregroundColor : titleColor],
                                          range: NSMakeRange(0, title.count))
        }
        self.setValue(attributeString, forKey: "attributedTitle")
    }
    
    private func setMessage(font: UIFont?, color: UIColor?) {
        guard let message = self.message else { return }
        let attributeString = NSMutableAttributedString(string: message)
        if let messageFont = font {
            attributeString.addAttributes([NSAttributedString.Key.font : messageFont],
                                          range: NSMakeRange(0, message.count))
        }
        
        if let messageColorColor = color {
            attributeString.addAttributes([NSAttributedString.Key.foregroundColor : messageColorColor],
                                          range: NSMakeRange(0, message.count))
        }
        self.setValue(attributeString, forKey: "attributedMessage")
    }
}
