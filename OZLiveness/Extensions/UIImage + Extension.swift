//
//  UIImage + Extension.swift
//  OZLiveness
//
//  Created by Ilya Postanogov on 27.01.2022.
//  Copyright © 2022 Igor Ovchinnikov. All rights reserved.
//
import UIKit
import Foundation

extension CIImage {
    var cgImageGen: CGImage? {
        let context = CIContext(options: nil)
        if let cgImage = context.createCGImage(self, from: self.extent) {
            return cgImage
        }
        return nil
    }
}
