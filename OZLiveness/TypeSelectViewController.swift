//
//  TypeSelectViewController.swift
//  OZLiveness
//
//  Created by Ilya Postanogov on 26.01.2022.
//  Copyright © 2022 Igor Ovchinnikov. All rights reserved.
//

import Foundation
import UIKit

class TypeSelectViewController: UIViewController {
    var currentType: AnalysisActionType = .onDevice
    @IBAction func didTapOnDevice(_ sender: Any) {
        currentType = .onDevice
        self.performSegue(withIdentifier: "showMainSegueID", sender: self)
    }
    
    @IBAction func didTapServerBased(_ sender: Any) {
        currentType = .serverBased
        self.performSegue(withIdentifier: "showMainSegueID", sender: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        switch segue.identifier {
        case "showMainSegueID":
            let vc = segue.destination as? ActionTableViewController
            vc?.mode = currentType
        default:()
        }
    }
}
