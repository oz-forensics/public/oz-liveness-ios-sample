//
//  BioVerificationViewController.swift
//  OZLiveness
//
//  Created by  Oz Forensics LLC. on 24.01.2022.
//  Copyright © 2022  Oz Forensics LLC. All rights reserved.
//

import Foundation
import UIKit
import YPImagePicker
import OZLivenessSDK
import Toaster
import ImageIO
import CoreImage
import AVFoundation
import MobileCoreServices

class BioVerificationViewController: UIViewController {
    @IBOutlet private var verificationView: UIView!
    @IBOutlet private var selectPhotoView: UIView!
    @IBOutlet private var photoImageView: UIImageView!
    @IBOutlet private var hoverView: UIView!
    
    private var referencePhoto: UIImage?
    private var request: OZLivenessSDK.OZCancelableRequest?
    
    var mode: AnalysisActionType = .onDevice
    var imagePicker: YPImagePicker?
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        request?.cancel()
        request = nil
    }
    
    //MARK: - Private methods
    private func setupUI() {
        
        hoverView.isHidden = false
        
        OZSDK.attemptSettings = OZAttemptSettings(singleCount: 2,
                                                  commonCount: 3)
        
        self.navigationItem.title = "Biometric verification"
        self.view.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 0.9529411765, alpha: 1)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapSelectPhotoView))
        selectPhotoView.isUserInteractionEnabled = true
        selectPhotoView.addGestureRecognizer(tapGesture)
        
        let tapCheckGesture = UITapGestureRecognizer(target: self, action: #selector(didTapCheckPhotoView))
        verificationView.isUserInteractionEnabled = true
        verificationView.addGestureRecognizer(tapCheckGesture)
    }
    
    @objc private func didTapCheckPhotoView() {
        do {
            let ozLivenessVC : UIViewController
            ozLivenessVC = try OZSDK.createVerificationVCWithDelegate(self, actions: [.selfie])
            self.present(ozLivenessVC, animated: true)
            UIApplication.shared.isIdleTimerDisabled = true
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    @objc private func didTapSelectPhotoView() {
        
        var config = YPImagePickerConfiguration()
        config.usesFrontCamera = true
        config.showsPhotoFilters = false
        config.showsVideoTrimmer = false
        config.screens = [.library, .photo]
        config.startOnScreen = YPPickerScreen.photo
        config.gallery.hidesRemoveButton = true
        config.library.maxNumberOfItems = 1
        config.library.isSquareByDefault = false
        
        imagePicker = YPImagePicker(configuration: config)
        
        imagePicker?.didFinishPicking { [unowned imagePicker] items, _ in
            if let photo = items.singlePhoto {
                self.hoverView.isHidden = true
                
                self.referencePhoto = photo.image
                self.photoImageView.image = photo.image
            }
            imagePicker?.dismiss(animated: true, completion: nil)
        }
        present(imagePicker!, animated: true, completion: nil)
    }
    
    private func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    private func savePNG(_ image: CGImage, url: CFURL, properties: CFDictionary? = nil) {
        guard let myImageDest = CGImageDestinationCreateWithURL(url, kUTTypePNG, 1, nil) else { return }
        CGImageDestinationAddImage(myImageDest, image, properties)
        CGImageDestinationFinalize(myImageDest)
    }
    
    private func saveFile(image: UIImage) -> URL? {
        guard let ciImage = CIImage(image: image) else {
            return nil
        }
        
        let filename = getDocumentsDirectory().appendingPathComponent("reference_photo.png")
        try? FileManager.default.removeItem(at: filename)
        savePNG(ciImage.cgImageGen!, url: filename as CFURL)
        return filename
    }
    
    func onDeviceLiveness(results: [OZMedia]) {
        guard let referencePhoto = referencePhoto else {
            return
        }
        
        guard let imageUrl = saveFile(image: referencePhoto) else {
            return
        }
        
        let referenceMedia = OZMedia.init(movement: .selfie,
                                   mediaType: .movement,
                                   metaData: nil,
                                   videoURL: nil,
                                   bestShotURL: imageUrl,
                                   preferredMediaURL: nil,
                                   timestamp: Date())
        
        var analysisMedia = [OZMedia]()
        analysisMedia.append(referenceMedia)
        analysisMedia.append(contentsOf: results)
        let activity = ProgressiveActivityView.show(in: self.view)
        let analysisRequest = AnalysisRequestBuilder()
        let analysisBiometry = Analysis.init(media: analysisMedia, type: .biometry, mode: .onDevice)
        analysisRequest.addAnalysis(analysisBiometry)

        analysisRequest.run { status in
            activity.progressView.startProgress(to: CGFloat((status.progressStatus?.progress.fractionCompleted ?? 0) * 100), duration: 0.1)
        } errorHandler: { error in
            print(error)
        } completionHandler: { results in
            activity.hide()
            var msg = ""
            results.analysisResults.forEach { res in
                msg = msg.isEmpty ? res.type : msg + ", " + res.type
            }
            let alertController = UIAlertController.createAlert(
                title: results.resolution.rawValue,
                message: msg
            )
            alertController.addAction(UIAlertAction(title: "Ok",
                                                    style: .cancel,
                                                    handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func serverBasedLiveness(results: [OZMedia]) {
        guard let referencePhoto = referencePhoto else {
            return
        }
        
        guard let imageUrl = saveFile(image: referencePhoto) else {
            return
        }
        
        let referenceMedia = OZMedia(movement: .selfie,
                                   mediaType: .movement,
                                   metaData: nil,
                                   videoURL: nil,
                                   bestShotURL: imageUrl,
                                   preferredMediaURL: nil,
                                   timestamp: Date())
        
        var analysisMedia = [OZMedia]()
        analysisMedia.append(referenceMedia)
        analysisMedia.append(contentsOf: results)
        let activity = ProgressiveActivityView.show(in: self.view)
        let analysisRequest = AnalysisRequestBuilder()
        let analysis = Analysis(media: analysisMedia, type: .quality, mode: .serverBased)
        let analysisBiometry = Analysis(media: analysisMedia, type: .biometry, mode: .serverBased)
        analysisRequest.addAnalysis(analysis)
        analysisRequest.addAnalysis(analysisBiometry)
        
        analysisRequest.run { status in
            activity.progressView.startProgress(to: CGFloat((status.progressStatus?.progress.fractionCompleted ?? 0) * 100), duration: 0.1)
        } errorHandler: { error in
            print(error)
        } completionHandler: { results in
            OZSDK.cleanTempDirectory()
            activity.hide()
            var msg = ""
            results.analysisResults.forEach { res in
                msg = msg.isEmpty ? res.type : msg + ", " + res.type
            }
            let alertController = UIAlertController.createAlert(
                title: results.resolution.rawValue,
                message: msg
            )
            alertController.addAction(UIAlertAction(title: "Ok",
                                                    style: .cancel,
                                                    handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
    }
}

extension BioVerificationViewController: OZLivenessDelegate {
    func onError(status: OZVerificationStatus?) {
        switch status {
        case .failedBecauseUserCancelled:
            let alertController = UIAlertController.createAlert(
                title:      localized("attention"),
                message:    localized("ActionTableViewController.Alert.FailedBecauseUserCancelled.Message")
            )
            alertController.addAction(UIAlertAction(title: localized("ok"),
                                                    style: .cancel,
                                                    handler: nil))
            self.present(alertController, animated: true, completion: { })
        case .failedBecauseOfAttemptLimit:
            let alertController = UIAlertController.createAlert(
                title:      localized("attention"),
                message:    localized("ActionTableViewController.Alert.FailedBecauseOfAttemptLimit.Message")
            )
            alertController.addAction(UIAlertAction(title: localized("ok"),
                                                    style: .cancel,
                                                    handler: nil))
            self.present(alertController, animated: true, completion: { })
        default: ()
        }
    }
    
    func onOZLivenessResult(results: [OZMedia]) {
        UIApplication.shared.isIdleTimerDisabled = false
        if mode == .onDevice {
            self.onDeviceLiveness(results: results)
        } else {
            self.serverBasedLiveness(results: results)
        }
    }
}
