//
//  ProgressiveActivityView.swift
//  OZLiveness
//
//  Created by Ilya Postanogov on 21.01.2022.
//  Copyright © 2022 Oz Forensics LLC. All rights reserved.
//

import Foundation
import UIKit
import UICircularProgressRing

class ProgressiveActivityView: UIView {
    @IBOutlet weak var thanksLabel:     UILabel!
    @IBOutlet weak var commentLabel:    UILabel!
    @IBOutlet weak var statusLabel:     UILabel!
    @IBOutlet weak var progressView:    UICircularProgressRing!
    
    private func pLocalization() {
        thanksLabel.text = "Thanks"
        commentLabel.text = "Please wait a few seconds"
        statusLabel.text = "Uploading documents"
    }
    
    class var objectFromNib: ProgressiveActivityView {
        get {
            let nibViews = Bundle.main.loadNibNamed("ProgressiveActivityView", owner: self, options: nil)
            let view = nibViews![0] as! ProgressiveActivityView
            view.pLocalization()
            return view
        }
    }
    
    class func show(in view: UIView) -> ProgressiveActivityView {
        let av = self.objectFromNib
        av.frame = view.bounds
        av.progressView.maxValue = 110
        av.progressView.value = 0
        view.addSubview(av)
        return av
    }
    
    class func show(in view: UIView, status: String, thanksIsHidden: Bool) -> ProgressiveActivityView {
        let av = self.objectFromNib
        av.frame = view.bounds
        av.progressView.maxValue = 110
        av.progressView.value = 0
        av.statusLabel.text = status
        av.thanksLabel.isHidden = thanksIsHidden
        view.addSubview(av)
        return av
    }
    
    func hide() {
        self.removeFromSuperview()
    }
}

