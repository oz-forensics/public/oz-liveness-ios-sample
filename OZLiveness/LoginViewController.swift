//
//  LoginViewController.swift
//  OZLiveness
//
//  Created by Oz Forensics LLC. on 12/08/2019.
//  Copyright © 2019 Oz Forensics LLC. All rights reserved.
//

import UIKit
import OZLivenessSDK


class LoginViewController: UITableViewController {

    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBAction func loginAction(_ sender: Any) {
        
        if  let login = loginTextField.text,        login.count > 0,
            let password = passwordTextField.text,  password.count > 0 {
            let activity = UIElements.activityView
            self.view.addSubview(activity)
            self.tableView.endEditing(true)
            OZSDK.setApiConnection(.fromCredentials(host: "https://sandbox.ohio.ozforensics.com", login: login, password: password)) { (token, error) in
                activity.removeFromSuperview()
                if let _ = token {
                    Router.showActionsController()
                }
                else {
                    self.showErrorAlert(error?.localizedDescription)
                }
            }
        }
        else {
            self.showErrorAlert(localized("LoginViewController.Alert.InputLoginAndPassword"))
        }
    }
    
    func showErrorAlert(_ text: String? = nil) {
        let alertController = UIAlertController(title: localized("LoginViewController.Alert.ErrorTitle"),
                                                message: text ?? localized("LoginViewController.Alert.Message"),
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: localized("LoginViewController.Alert.Ok"),
                                                style: .cancel,
                                                handler: nil))
        self.present(alertController, animated: true, completion: { })
    }
}
